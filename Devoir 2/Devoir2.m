function [coup vbf t rbt]= Devoir2 (option,xy0,vb0,wb0)
    % Constants
    %% Ball Radius
    ballRadius = 21.35 / 1000; % m

    % Variables
    coup = 0;
    currentSpeed = vb0';
    Rarr = zeros(3000000, 3);
    Rarr(1, 1) = xy0(1);
    Rarr(1, 2) = xy0(2);
    Rarr(1, 3) = ballRadius;
    Tarr(1) = 0;
    it = 2; % start at 2 because we're doing maualy the first iteration.
    while true
        
        %3 : Calculate acceleration
        acceleration = CalculateAcc(option, currentSpeed, wb0);
        deltaT =1;
        m=1;
        deltaD = abs(currentSpeed * deltaT + 0.5 * acceleration * deltaT^2);
        while sqrt(deltaD(1)^2 + deltaD(2)^2 + deltaD(3)^2) > 0.001
            m = m + 1;
           deltaT = deltaT / 2^(m-1);
           deltaD = abs(currentSpeed * deltaT + 0.5 * acceleration * deltaT^2);
        end
        %4 : Calculate new position and new speed
        Rarr(it, :) = Rarr(it-1, :) + currentSpeed * deltaT + 0.5 * acceleration * deltaT^2;
        Tarr(it) = Tarr(it-1) + deltaT;
        %5 : Calculate new speed
        currentSpeed = currentSpeed + acceleration * deltaT;

        coup = EndingSpenario(Rarr(it, :));
        it = it + 1;
        if (coup ~= -1)
            break
        end 
    end 

    rbt = Rarr;
    t = Tarr';
    vbf = currentSpeed;
end

function [coup]=EndingSpenario(r)
    % Constants
    %% Ball Radius
    ballRadius = 21.35 / 1000; % m
    curveHeight = 3.5; % m 

    %Variables
    coup = -1;
    x = r(1);
    y = r(2);
    z = r(3);


    % Verification 1
    if (z <= ballRadius && x >= 0 && x <= 60 && y >= 0 && y <= 70 )
        coup = 2;
    % Verification 2
    elseif ( x < 0 || x > 110 || y < 0 || y > 70 || y  < (9/10 * x - 54 ) )
        coup = 3;
    % Verification 3
    elseif ( z <= curveHeight && ((y - 53)^2 + (x- 92)^2) < 0.054^2 )
        coup = 0;
    % Verification 4
    elseif ( ( (y - 53)^2 + (x - 92)^2 ) < 15^2 && ( z <= sqrt(33.9^2 - (x-92)^2 - (y-53)^2) -30.4 ))
        coup = 1;
    % Verification 5
    elseif ( z <= ballRadius )
        coup = 2;
    end  
end

function [acc]=CalculateAcc(option,v,w)
    % Constants
    %% Mass of the golf ball
    ballMass = 45.9 / 1000; % Kg
    airDensity = 1.2; % Kg/m^3
    frictionConstant = 0.14;
    ballRadius = 21.35 / 1000; % m
    effectiveSurface = pi * ballRadius^2; % m^2

    magnusConstant = 0.000791 * norm(w);
    

    %% Gravity Constant acceleration
    gravity = [0,0,- 9.8];

    % Variables
    %% Acceleration
    acc = [0,0,0];

    % Add Gravity
    acc = acc + gravity;

    % If the index is 1 or invalid just return gravity
    if (option < 2 || option > 3)
        return
    end

    % Add Friction
    friction = -0.5 * airDensity * frictionConstant * effectiveSurface * norm(v) * v;
    acc = acc + friction / ballMass;

    if (option == 2)
        return
    end

    % Add Magnus
    magnus = 0.5 * airDensity * magnusConstant * effectiveSurface * norm(v)^2 * (cross(w',v)/norm(cross(w',v)));
    acc = acc + magnus / ballMass;    

end